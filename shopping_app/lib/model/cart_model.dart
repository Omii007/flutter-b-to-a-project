class CartModel {
  String? image;
  String? shirtName;
  String? color;
  int? size;
  double? price;
  int? quantity;

  CartModel({
    this.image,
    this.shirtName,
    this.color,
    this.size,
    this.price,
    this.quantity,
  });
}
