import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shopping_app/controller/grid_cart_controller.dart';
import 'package:shopping_app/view/screen3.dart';

import '../model/grid_cart_model.dart';

class Screen1 extends StatefulWidget {
  const Screen1({super.key});

  @override
  State<Screen1> createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  int selectedIndex = 0;
  int selectedPageIndex = 0;
  int? selectedCart;

  void selectPage(int index) {
    setState(() {
      selectedPageIndex = index;
    });
  }

  final List<String> categories = [
    "All",
    "Men",
    "Women",
    "Kids",
    "Other",
  ];

  List<GridCartModel> gridList = [];

  @override
  void initState() {
    super.initState();
    gridList = Provider.of<GridCartController>(context, listen: false).gridList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 30.0),
          child: GestureDetector(
            onTap: () {},
            child: Image.asset(
              "assets/icons/Menu (1).png",
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 30.0),
            child: GestureDetector(
              onTap: () {},
              child: Image.asset(
                "assets/icons/Profile (1).png",
              ),
            ),
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0),
            child: Text(
              "Explore",
              style: GoogleFonts.imprima(
                fontWeight: FontWeight.w400,
                fontSize: 36,
                color: const Color.fromRGBO(13, 13, 13, 1),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0),
            child: Text(
              "Best trendy collection!",
              style: GoogleFonts.imprima(
                fontWeight: FontWeight.w400,
                fontSize: 18,
                color: const Color.fromRGBO(121, 119, 128, 1),
              ),
            ),
          ),
          const SizedBox(
            height: 26,
          ),
          SizedBox(
            height: 32,
            // width: 54,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              // shrinkWrap: true,
              // physics: const NeverScrollableScrollPhysics(),
              itemCount: categories.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedIndex = index;
                    });
                  },
                  child: Container(
                    margin: const EdgeInsets.only(left: 30),
                    height: 32,
                    width: 54,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32),
                      color: selectedIndex == index
                          ? const Color.fromRGBO(255, 122, 0, 1)
                          : Colors.transparent,
                    ),
                    child: Center(
                      child: Text(
                        categories[index],
                        style: GoogleFonts.imprima(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == index
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          const SizedBox(
            height: 26,
          ),
          Expanded(
            child: Center(
              child: GridView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 2 / 3,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
                itemCount: gridList.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      selectedCart = index;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const Screen3(),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        border: null,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Image.asset(
                                gridList[index].image ?? '',
                                width: 150,
                              ),
                              Positioned(
                                bottom: -15,
                                right: 15,
                                child: Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color:
                                        const Color.fromRGBO(255, 255, 255, 1),
                                    border: Border.all(
                                      color: const Color.fromRGBO(
                                          255, 255, 255, 1),
                                    ),
                                  ),
                                  child: Container(
                                    margin: const EdgeInsets.all(5),
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color:
                                          const Color.fromRGBO(13, 13, 14, 1),
                                      border: Border.all(
                                        color:
                                            const Color.fromRGBO(13, 13, 14, 1),
                                      ),
                                    ),
                                    child: const Icon(
                                      Icons.shopping_bag_outlined,
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      size: 11,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "\$${gridList[index].price}",
                            style: GoogleFonts.imprima(
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                              color: const Color.fromRGBO(13, 13, 14, 1),
                            ),
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          Text(
                            "${gridList[index].name}",
                            style: GoogleFonts.imprima(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: const Color.fromRGBO(121, 119, 128, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 20,
        selectedFontSize: 12,
        elevation: 0,
        onTap: selectPage,
        unselectedFontSize: 12,
        showUnselectedLabels: true,
        useLegacyColorScheme: false,
        unselectedItemColor: const Color.fromRGBO(13, 13, 14, 1),
        currentIndex: selectedPageIndex,
        selectedItemColor: const Color.fromRGBO(230, 154, 21, 1),
        items: const [
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(255, 255, 255, 1),
            label: "Home",
            icon: Icon(
              Icons.home_outlined,
            ),
          ),
          BottomNavigationBarItem(
            label: "Search",
            icon: Icon(
              Icons.search,
            ),
          ),
          BottomNavigationBarItem(
            label: "Cart",
            icon: Icon(
              Icons.card_travel_rounded,
            ),
          ),
          BottomNavigationBarItem(
            label: "Setting",
            icon: Icon(
              Icons.settings,
            ),
          ),
        ],
      ),
    );
  }
}
