import 'package:flutter/material.dart';

import '../model/cart_model.dart';

class CartController with ChangeNotifier {
  List<CartModel> cartList = [
    CartModel(
      image: "assets/images/s_image_2.png",
      shirtName: "Premium Tagerine Shirt",
      color: "Yellow",
      size: 8,
      price: 257.85,
      quantity: 1,
    ),
    CartModel(
      image: "assets/images/s_image_5.png",
      shirtName: "Leather Tagerine Coart",
      color: "Yellow",
      size: 8,
      price: 257.85,
      quantity: 1,
    ),
  ];
}
