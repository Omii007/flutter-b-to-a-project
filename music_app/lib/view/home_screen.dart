import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/view/screen1.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
      body: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: Column(
              children: [
                Image.asset(
                  "assets/images/music.png",
                  filterQuality: FilterQuality.high,
                  fit: BoxFit.fitWidth,
                ),
              ],
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 2,
            left: MediaQuery.of(context).size.width / 7.5,
            child: Center(
              child: SizedBox(
                height: 150,
                width: 314,
                child: Text(
                  "Dancing between\nThe shadows\nOf rhythm",
                  textAlign: TextAlign.start,
                  style: GoogleFonts.inter(
                    fontWeight: FontWeight.w600,
                    color: const Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 36,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 1.4,
            left: MediaQuery.of(context).size.width / 6,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Screen1(),
                  ),
                );
              },
              child: Container(
                height: 47,
                width: MediaQuery.of(context).size.width / 1.5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(19),
                  color: const Color.fromRGBO(255, 46, 0, 1),
                ),
                child: Center(
                  child: Text(
                    "Get started",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(0, 0, 0, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 1.25,
            left: MediaQuery.of(context).size.width / 6,
            child: GestureDetector(
              onTap: () {},
              child: Container(
                height: 47,
                width: MediaQuery.of(context).size.width / 1.5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(19),
                  color: const Color.fromRGBO(19, 19, 19, 1),
                  border: Border.all(
                    color: const Color.fromRGBO(255, 61, 0, 1),
                  ),
                ),
                child: Center(
                  child: Text(
                    "Continue with Email",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(255, 46, 0, 1),
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 1.15,
            left: MediaQuery.of(context).size.width * 0.2,
            child: SizedBox(
              width: 250,
              child: Text(
                "by continuing you agree to terms of services and  Privacy policy",
                textAlign: TextAlign.center,
                overflow: TextOverflow.clip,
                style: GoogleFonts.inter(
                  fontWeight: FontWeight.w600,
                  color: const Color.fromRGBO(203, 200, 200, 1),
                  fontSize: 14,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
