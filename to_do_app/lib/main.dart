import 'package:flutter/material.dart';
import 'package:to_do_app/login.dart';
import 'package:to_do_app/register.dart';
import 'model.dart';

void main() async{
  runApp(const MainApp());
  
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Login(registerModelClass:
        RegisterModelClass(
          username: "", 
          password: "",
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
