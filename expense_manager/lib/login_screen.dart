
import 'package:expense_manager/register_screen.dart';
import 'package:expense_manager/transaction_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State createState()=> _LoginScreenState();
}

class _LoginScreenState extends State <LoginScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 86,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/images/expense_splash.png",
                height: 65,
                width: 75,
              ),
            ],
          ),
          const SizedBox(
            height: 60.18,
          ),
      
          Padding(
            padding: const EdgeInsets.only(left: 30.0,right: 30),
            child: SizedBox(
              // height: 300,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Login to your Account",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w500,
                      fontSize:16,
                      color:const Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 3),
                          blurRadius: 10,
                          color: Color.fromRGBO(0, 0, 0, 0.15),
                        ),
                      ],
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        fillColor: const Color.fromRGBO(255,255,255,1),
                        filled: true,
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        hintText: "Username",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:12,
                          color:const Color.fromRGBO(0,0,0,0.4),
                        ),
                      ),
                    ),
                  ),
            
                  // 2
                  const SizedBox(
                    height: 22,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 3),
                          blurRadius: 10,
                          color: Color.fromRGBO(0, 0, 0, 0.15),
                        ),
                      ],
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        fillColor: const Color.fromRGBO(255,255,255,1),
                        filled: true,
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        hintText: "Password",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:12,
                          color:const Color.fromRGBO(0,0,0,0.4),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
            
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context)=> const TransactionScreen(),
                        )
                      );
                    },
                    child: Container(
                      height: 49,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: const Color.fromRGBO(14, 161, 125, 1),
                      ),
                      child: Center(
                        child: Text("Sign In ",
                          style: GoogleFonts.poppins(
                            fontWeight:FontWeight.w500,
                            fontSize:15,
                            color:const Color.fromRGBO(255,255,255,1),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Don’t have an account? ",
                style: GoogleFonts.poppins(
                  fontWeight:FontWeight.w400,
                  fontSize:12,
                  color:const Color.fromRGBO(0,0,0,0.6),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context)=> const RegisterScreen(),
                    )
                  );
                },
                child: Text("Sign up ",
                  style: GoogleFonts.poppins(
                    fontWeight:FontWeight.w400,
                    fontSize:12,
                    color:const Color.fromRGBO(14, 161, 125, 1),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }
}