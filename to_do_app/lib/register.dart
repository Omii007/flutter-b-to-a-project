
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:to_do_app/login.dart';
import 'package:to_do_app/root.dart';
import 'package:sqflite/sqflite.dart';

class Register extends StatefulWidget {
  const Register({super.key});

  @override
  State createState()=> _RegisterState();
}

class RegisterModelClass{
  int? id;
  String username;
  String password;

  RegisterModelClass({
    this.id,
    required this.username,
    required this.password,
  });

  Map<String,dynamic> registerMap(){
    return {
      'id': id,
      'username': username, 
      'password': password,
    };
  }

  @override
  String toString(){
    return "{id: $id, username: $username, password: $password}";
  }
}

class _RegisterState extends State<Register> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmpasswordController = TextEditingController();
  bool flag = true;
  bool insertFlag = false;
  List registerList = [];
  @override
  void initState() {
    super.initState();
    initializedDatabase();
  }

  Future<void> initializedDatabase()async{
    registerList = await activeDataBase();
    setState(() {});
  }

  Future<List<RegisterModelClass>> activeDataBase()async {
    List<RegisterModelClass> initialList = await getDataBase();
    return initialList;
  }

  Future<void> insertData(RegisterModelClass obj)async {
    final localDB = await database;

    await localDB.insert(
      "RegisterDataBase",
      obj.registerMap(),
      conflictAlgorithm:ConflictAlgorithm.replace,
    );
    registerList = await getDataBase();
    insertFlag = true;
    setState(() {});
    print("Data inserted in database");
  }
  
  Future<List<RegisterModelClass>> getDataBase()async {
    final localDB = await database;

    List<Map<String,dynamic>> mapList = await localDB.query("RegisterDataBase");
    return List.generate(mapList.length, (index) {
      return RegisterModelClass(
        id: mapList[index]['id'],
        username: mapList[index]['username'], 
        password: mapList[index]['password']
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  image: DecorationImage(image: AssetImage(
                    "assets/images/todo1.jpg",
                    ),
                    fit: BoxFit.fill,
                    filterQuality: FilterQuality.high,
                  ),
                ),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 100,left: 10,right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Sign Up",
                              style: GoogleFonts.inter(
                                fontWeight:FontWeight.bold,
                                fontSize:40,
                                color:Colors.black,
                              ),
                            ),
                          ],
                        ),
        
                        const SizedBox(
                          height: 30,
                        ),
                        Text("Username",
                          style: GoogleFonts.inter(
                            fontWeight:FontWeight.normal,
                            fontSize:17,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
        
                        const SizedBox(
                          height: 4,
                        ),
                        TextFormField(
                          controller: _usernameController,
                          decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(color: Colors.black,width: 3)
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                color: Colors.black,                 
                              ),
                            ),
                            hintText: "Enter username",
                            hintStyle: GoogleFonts.inter(
                              fontWeight:FontWeight.normal,
                              fontSize:15,
                              color:const Color.fromRGBO(0,0,0,0.7),
                            ),
                          ),
                          validator: (value) {
                            if(value!.trim().isEmpty){
                              return "Please enter username";
                            }else if(value.length < 6){
                              return "Username must be 6 character";
                            }else{
                              return null;
                            }
                          },
                        ),

                        // password TextFormField
                        const SizedBox(
                          height: 15,
                        ),
                        Text("Password",
                          style: GoogleFonts.inter(
                            fontWeight:FontWeight.normal,
                            fontSize:17,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
        
                        const SizedBox(
                          height: 4,
                        ),
                        TextFormField(
                          controller: _passwordController,
                          obscureText: flag,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: (){
                                setState(() {
                                  flag = !flag;
                                });
                              }, 
                              icon: Icon(
                                flag? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(color: Colors.black,width: 3)
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                color: Colors.black,                 
                              ),
                            ),
                            hintText: "Enter password",
                            hintStyle: GoogleFonts.inter(
                              fontWeight:FontWeight.normal,
                              fontSize:15,
                              color:const Color.fromRGBO(0,0,0,0.7),
                            ),
                          ),
                          validator: (value) {
                            if(value!.trim().isEmpty){
                              return "Please enter password";
                            }else if(value.length < 7){
                              return "Username must be 8 character";
                            }else if(!value.contains(RegExp(r'[A-Z]'))){
                              return "Password must be contains one upper case letter";
                            }else if(!value.contains(RegExp(r'[a-z]'))){
                              return "Password must be contains one lower case letter";
                            }else if(!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))){
                              return "Password must be contains one special character";
                            }else{
                              return null;
                            }
                          },
                        ),

                        // confirm password

                        const SizedBox(
                          height: 15,
                        ),
                        Text("Confirm Password",
                          style: GoogleFonts.inter(
                            fontWeight:FontWeight.normal,
                            fontSize:17,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
        
                        const SizedBox(
                          height: 4,
                        ),
                        TextFormField(
                          controller: _confirmpasswordController,
                          obscureText: flag,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: (){
                                setState(() {
                                  flag = !flag;
                                });
                              }, 
                              icon: Icon(
                                flag? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(color: Colors.black,width: 3)
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                color: Colors.black,                 
                              ),
                            ),
                            hintText: "Enter password",
                            hintStyle: GoogleFonts.inter(
                              fontWeight:FontWeight.normal,
                              fontSize:15,
                              color:const Color.fromRGBO(0,0,0,0.7),
                            ),
                          ),
                          validator: (value) {
                            if(value!.trim().isEmpty){
                              return "Please enter confirm password";

                            }else if(_passwordController.text != _confirmpasswordController.text){
                              return "Password not match";
                            // }else if(value.length < 7){
                            //   return "Username must be 8 character";
                            // }else if(!value.contains(RegExp(r'[A-Z]'))){
                            //   return "Password must be contains one upper case letter";
                            // }else if(!value.contains(RegExp(r'[a-z]'))){
                            //   return "Password must be contains one lower case letter";
                            // }else if(!value.contains(r'[!@#$%^&*(),.?":{}|<>]')){
                            //   return "Password must be contains one special character";
                            }else{
                              return null;
                            }
                          },
                        ),

                        const SizedBox(
                          height: 40,
                        ),  
                        // Elevated Button

                        GestureDetector(
                          onTap: () {
                            setState(() {
                              bool validated = _formKey.currentState!.validate();

                              if(_usernameController.text.trim().isNotEmpty && 
                                _passwordController.text.trim().isNotEmpty && 
                                _confirmpasswordController.text.trim().isNotEmpty && validated
                              ){
                                setState(() async{
                                  await insertData(
                                    RegisterModelClass(
                                      username: _usernameController.text, 
                                      password: _passwordController.text,
                                    ),
                                  );
                                });

                                if(insertFlag){

                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text("Sign Up Successful",
                                        style: GoogleFonts.inter(
                                          fontSize:20,
                                          color:Colors.black,
                                          fontWeight:FontWeight.bold,
                                        ),
                                      ),
                                      backgroundColor: Colors.green,  
                                    ),
                                  );

                                  Navigator.push(
                                    context, MaterialPageRoute(
                                      builder: (context)=>  Login(registerModelClass:
                                        RegisterModelClass(
                                          username: _usernameController.text, 
                                          password: _passwordController.text)
                                      ),
                                    ),
                                  );
                                }else{
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text("Sign Up Failed",
                                        style: GoogleFonts.inter(
                                          fontSize:20,
                                          color:Colors.black,
                                          fontWeight:FontWeight.bold,
                                        ),
                                      ),
                                      backgroundColor: Colors.red,  
                                    ),
                                  );
                                }
                                
                              }
                            });
                          },
                          child: Container(
                            height: 50,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.purpleAccent,
                            ),
                            child: Center(
                              child: Text("Sign Up",                        
                              textAlign: TextAlign.center,
                                style: GoogleFonts.inter(                                 
                                  fontWeight:FontWeight.bold,
                                  fontSize:25,
                                  color:Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),

                        const SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Already have an account? ",
                              style: GoogleFonts.inter(
                                fontWeight:FontWeight.normal,
                                fontSize:15,
                                color:Colors.black,
                              ),
                            ),
                            TextButton(
                              onPressed: (){
                                Navigator.push(
                                  context,MaterialPageRoute(
                                    builder: (context)=>  Login(registerModelClass:
                                      RegisterModelClass(
                                        username: _usernameController.text, 
                                        password: _passwordController.text,
                                      )
                                    ),
                                  ),
                                );
                              }, 
                              child: Text("Login",
                                style: GoogleFonts.inter(
                                  fontWeight:FontWeight.normal,
                                  fontSize:15,
                                  color:Colors.red,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}