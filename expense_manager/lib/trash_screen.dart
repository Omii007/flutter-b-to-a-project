

import 'package:expense_manager/drawer_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TrashScreen extends StatefulWidget {

  const TrashScreen({super.key});

  @override
  State createState()=> _TrashScreenState();
}

class _TrashScreenState extends State<TrashScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /// DRAWER FOR MORE OPTIONS
      drawer: const MyDrawer(),
      appBar: AppBar(
        titleSpacing: 22,
        title: Text("Trash",
          style: GoogleFonts.poppins(
            fontWeight:FontWeight.w500,
            fontSize:16,
            color:const Color.fromRGBO(33,33,33,1),
          ),
        ),
        actions: [
          IconButton(
            onPressed: (){}, 
            icon: const Icon(Icons.search_rounded,size: 24,),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: 3,
        itemBuilder: (BuildContext context,int index){
          return Column(
            children: [
              SizedBox(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 7,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [                   
                        SizedBox(
                          height: 21,
                          width: 21,
                          child: IconButton(
                            onPressed:(){}, 
                            icon: const Icon(
                              Icons.remove_circle_rounded,
                              color: Color.fromRGBO(204, 210, 227, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                child: Text("Medicine",
                                  style: GoogleFonts.poppins(
                                    fontWeight:FontWeight.w400,
                                    fontSize:15,
                                    color:const Color.fromRGBO(0,0,0,1),
                                  ),
                                ),
                              ),
                              const Spacer(),
                              Text("500",                          
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:15,
                                color:const Color.fromRGBO(0,0,0,1),
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            ],
                          ),
                          SizedBox(
                            width: 251,
                            child: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry... more",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                          ),
                          
                        
                        Row(
                          children: [
                            const Spacer(),
                            Text("11 April",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                            Text(" | ",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                            Text("11:40 AM",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        ],
                      ),
                    ),
                    // Column(
                    //   children: [
                        
                    //   ],
                    // ),
                    
                  ],
                ),
              ),
              const Divider(
                indent: 10,
                height: 10,
                thickness: 1,
                color: Color.fromRGBO(206, 206, 206, 1),
              ),
            ],
          );
        },
      ),
    );
  }
  
}

