import 'package:flutter/material.dart';

import '../model/nearlocation_model.dart';

class NearLocationController with ChangeNotifier {
  List<NearLocationModel> nearHomeList = [
    NearLocationModel(
      image: "assets/images/home_5.png",
      name: "Jumeriah Golf Estates Villa",
      location: "London,Area Plam Jumeriah",
      bedRoom: 4,
      bathRoom: 5,
      price: 4500,
    ),
    NearLocationModel(
      image: "assets/images/home_5.png",
      name: "Jumeriah Golf Estates Villa",
      location: "London,Area Plam Jumeriah",
      bedRoom: 4,
      bathRoom: 5,
      price: 4500,
    ),
    NearLocationModel(
      image: "assets/images/home_5.png",
      name: "Jumeriah Golf Estates Villa",
      location: "London,Area Plam Jumeriah",
      bedRoom: 4,
      bathRoom: 5,
      price: 4500,
    ),
    NearLocationModel(
      image: "assets/images/home_5.png",
      name: "Jumeriah Golf Estates Villa",
      location: "London,Area Plam Jumeriah",
      bedRoom: 4,
      bathRoom: 5,
      price: 4500,
    ),
  ];
}
