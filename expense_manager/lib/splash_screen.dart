
import 'package:expense_manager/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State createState()=> _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 4),(){
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (context)=> const LoginScreen(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(),
          Center(
            child: GestureDetector(
              onTap: () {
                // Future.delayed(const Duration(seconds: 4),(){
                //   Navigator.push(
                //     context, 
                //     MaterialPageRoute(
                //       builder: (context)=> const LoginScreen(),
                //     ),
                //   );
                // }
                // );
              },
              child: Container(
                //margin: const EdgeInsets.only(top: 329),
                height: 144,
                width: 144,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(234, 238, 235, 1),
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/images/expense_splash.png",  
                    ),
                  ),
                ),
                // child: SvgPicture.asset(
                //   "assets/images/expense.svg",
                //   fit: BoxFit.fitHeight,
                //   height: 58.82,
                //   width: 69.76,
                // ),
              ),
            ),
          ),
          const Spacer(),
          Text("Expense Manager",
            style: GoogleFonts.poppins(
              fontWeight:FontWeight.w600,
              fontSize:16,
              color:const Color.fromRGBO(0,0,0,1),
            ),
          ),
          // const Spacer(),
          const SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }
}