
import 'package:expense_manager/drawer_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Category extends StatefulWidget {
  const Category({super.key});

  @override
  State createState()=> _CategoryState();
}

class CategoryModelClass {
  String image;
  String name;

  CategoryModelClass({
    required this.image,
    required this.name,
  });
}

class _CategoryState extends State<Category> {

  List imageList = [
    CategoryModelClass(
      image: "assets/images/food.png", 
      name: "Food"
    ),
    CategoryModelClass(
      image: "assets/images/fuel.png", 
      name: "Fuel"
    ),
    CategoryModelClass(
      image: "assets/images/medicine.png", 
      name: "Medicine"
    ),
    CategoryModelClass(
      image: "assets/images/shopping.png",
      name: "Shopping"
    ), 
  ];

  void showMyDialog(){
    showDialog(
      context: context,
      barrierDismissible: false, 
      builder: (context) {
        return AlertDialog(
        
          actions: [
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Text("Delete Category",
                style: GoogleFonts.poppins(
                  fontWeight:FontWeight.w500,
                  fontSize:16,
                  color:const Color.fromRGBO(0,0,0,1),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
              Center(
                child: SizedBox(
                  height: 36,
                  width: 215,
                  child: Center(
                    child: Text("Are you sure you want to delete the selected category?",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontWeight:FontWeight.w400,
                        fontSize:12,
                        color:const Color.fromRGBO(0,0,0,1),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  /// DELETE BUTTON
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: 35,
                      width: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: const Color.fromRGBO(14, 161, 125, 1),
                      ),
                      child: Center(
                        child: Text("Delete",
                          style: GoogleFonts.poppins(
                            fontWeight:FontWeight.w500,
                            fontSize:16,
                            color:const Color.fromRGBO(255,255,255,1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  /// CANCEL BUTTON
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: 35,
                      width: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: const Color.fromRGBO(140, 128, 128, 0.2),
                      ),
                      child: Center(
                        child: Text("Cancel",
                          style: GoogleFonts.poppins(
                            fontWeight:FontWeight.w500,
                            fontSize:16,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
          ],
          
        );
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyDrawer(),
      appBar: AppBar(
        title: Text("Categories",
          style: GoogleFonts.poppins(
            fontWeight:FontWeight.w500,
            fontSize:16,
            color:const Color.fromRGBO(33,33,33,1),
          ),
        ),
      ),
      body: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 23,
          mainAxisSpacing: 23,
        ),
        padding: const EdgeInsets.all(23),
        itemCount: imageList.length,
        itemBuilder: (context,index){
          return GestureDetector(
            onLongPress: () {
              showMyDialog();
            },
            child: Container(
              height: 150,
              width: 145,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: const Color.fromRGBO(255,255,255,1),
                boxShadow: const [
                  BoxShadow(
                    offset: Offset(1, 2),
                    blurRadius: 8,
                    color:Color.fromRGBO(0,0,0,0.15),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 34.0,right: 37,top: 19),
                    child: SizedBox(
                      height: 74,
                      width: 74,
                      child: Image.asset(
                        imageList[index].image,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Text(imageList[index].name,
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w500,
                      fontSize:16,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterFloat,
      floatingActionButton: Container(
        height: 46,
        width: 166,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(67),
        ),
        child: FloatingActionButton(
          onPressed: (){
            bottomSheet();
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(67),
          ),
          backgroundColor: const Color.fromRGBO(255,255,255,1),
          child: Row(
            children: [
              const SizedBox(
                width: 5,
              ),
              const Icon(Icons.add_circle_rounded,
                size: 32,
                color: Color.fromRGBO(14, 161, 125, 1),
              ),
              const SizedBox(
                width: 9,
              ),
              Text("Add Category",
                style: GoogleFonts.poppins(
                  fontWeight:FontWeight.w400,
                  fontSize:12,
                  color:const Color.fromRGBO(37,37,37,1),
                ),
              ),
            ],
          ),  
        ),
      ),
    );
  }

  void bottomSheet(){
    showModalBottomSheet(
      isScrollControlled: true,
      context: context, 
      backgroundColor: const Color.fromRGBO(255,255,255,1),
      builder: (context) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(22),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 97,
                        width: 74,
                        child: Column(
                          children: [
                            GestureDetector(
                              child: Container(
                                height: 74,
                                width: 74,
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color.fromRGBO(140, 128, 128, 0.2),
                                ),
                                child: Center(
                                  child: Image.asset(
                                    "assets/images/catimage.png",
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 3,
                            ),
                            Text("Add",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:13,
                                color:const Color.fromRGBO(0,0,0,1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Text("Image URL",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  // Date TextFormField
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(191, 189, 189, 1)
                          ),
                        ),
                        hintText: "Enter URL",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:13,
                          color:const Color.fromRGBO(0,0,0,0.8),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 19,
                  ),
              
                  // Amount
                  Text("Category",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  // Amount TextFormField
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(191, 189, 189, 1)
                          ),
                        ),
                        hintText: "Enter category name",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:13,
                          color:const Color.fromRGBO(0,0,0,0.8),
                        ),
                      ),
                    ),
                  ),
              
                  const SizedBox(
                    height: 34,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          
                        },
                        child: Container(
                          height: 40,
                          width: 123,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(67),
                            color: const Color.fromRGBO(14, 161, 125, 1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(1, 2),
                                blurRadius: 2,
                                color: Color.fromRGBO(0,0,0,0.2),
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text("Add",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w500,
                                fontSize:16,
                                color:const Color.fromRGBO(255,255,255,1),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}