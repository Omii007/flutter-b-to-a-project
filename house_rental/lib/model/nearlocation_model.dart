class NearLocationModel {
  String? image;
  String? name;
  String? location;
  int? bedRoom;
  int? bathRoom;
  int? price;

  NearLocationModel({
    this.image,
    this.name,
    this.location,
    this.bedRoom,
    this.bathRoom,
    this.price,
  });
}
