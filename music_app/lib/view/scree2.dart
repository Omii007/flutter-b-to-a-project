import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Screen2 extends StatefulWidget {
  const Screen2({super.key});

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  int selectedPageIndex = 0;
  void selectPage(int index) {
    setState(() {
      selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 1.5,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  color: Colors.black,
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/images/song_image.png",
                    ),
                    filterQuality: FilterQuality.high,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height / 1.7,
                left: MediaQuery.of(context).size.width / 4,
                child: Text(
                  "Alone in the Abyss",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.inter(
                    fontWeight: FontWeight.w400,
                    fontSize: 24,
                    color: const Color.fromRGBO(230, 154, 21, 1),
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height / 1.6,
                left: MediaQuery.of(context).size.width / 2.25,
                child: Center(
                  child: Text(
                    "Youlakou",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      fontSize: 13,
                      color: const Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height / 1.65,
                left: MediaQuery.of(context).size.width / 1.15,
                child: Center(
                  child: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.upload_file_outlined,
                      color: Color.fromRGBO(230, 154, 21, 1),
                      size: 25,
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 14,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 17.0),
            child: Row(
              children: [
                Text(
                  "Dynamic Warmup | ",
                  style: GoogleFonts.inter(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: const Color.fromRGBO(255, 255, 255, 1),
                  ),
                ),
                const Spacer(),
                Text(
                  "4 min",
                  style: GoogleFonts.inter(
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                    color: const Color.fromRGBO(255, 255, 255, 1),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14.0),
            child: LinearProgressIndicator(
              borderRadius: BorderRadius.circular(15),
              value: 25 / 100,
              backgroundColor: const Color.fromRGBO(217, 217, 217, 0.19),
              color: const Color.fromRGBO(230, 154, 21, 1),
              // valueColor: Color.fromRGBO(230, 154, 21, 1),
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {},
                child: Image.asset(
                  "assets/icons/repeat.png",
                  filterQuality: FilterQuality.high,
                  height: 24,
                  width: 24,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Image.asset(
                  "assets/icons/previous.png",
                  filterQuality: FilterQuality.high,
                  height: 25,
                  width: 25,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Image.asset(
                  "assets/icons/play.png",
                  filterQuality: FilterQuality.high,
                  height: 50,
                  width: 50,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Image.asset(
                  "assets/icons/next.png",
                  filterQuality: FilterQuality.high,
                  height: 25,
                  width: 25,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Image.asset(
                  "assets/icons/sound.png",
                  filterQuality: FilterQuality.high,
                  height: 24,
                  width: 24,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 24,
        selectedFontSize: 15,
        elevation: 0,
        onTap: selectPage,
        unselectedFontSize: 15,
        showUnselectedLabels: true,
        useLegacyColorScheme: false,
        unselectedItemColor: const Color.fromRGBO(157, 178, 206, 1),
        currentIndex: selectedPageIndex,
        selectedItemColor: const Color.fromRGBO(230, 154, 21, 1),
        items: const [
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(Icons.favorite_border_outlined),
            label: 'Favorite',
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(Icons.search_rounded),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(
              Icons.home_outlined,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(
              Icons.card_travel,
            ),
            label: "Cart",
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(
              Icons.person_3_outlined,
            ),
            label: "Profile",
          ),
        ],
      ),
    );
  }
}
