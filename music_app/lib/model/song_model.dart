class SongModel {
  String? image;
  String? songTitle;
  int? year;
  String? specialKey;
  String? subTitle;

  SongModel({
    this.image,
    this.songTitle,
    this.specialKey,
    this.subTitle,
    this.year,
  });
}
