import 'package:expense_manager/category_screen.dart';
import 'package:expense_manager/graph_screen.dart';
import 'package:expense_manager/trash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'transaction_screen.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({super.key});

  @override
  State createState()=> _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer>{

  bool _flag1 = true;
  bool _flag2 = true;
  bool _flag3 = true;
  bool _flag4 = true;
  bool _flag5 = true;


  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 18.0,top: 50),
              child: SizedBox(
                height: 24,
                width: 150,
                child: Text("Expense Manager",
                  style: GoogleFonts.poppins(
                    fontWeight:FontWeight.w600,
                    fontSize:16,
                    color:const Color.fromRGBO(0,0,0,1),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 18.0),
              child: SizedBox(
                height: 15,
                width: 150,
                child: Text("Saves all your Transactions",
                  style: GoogleFonts.poppins(
                    fontWeight:FontWeight.w400,
                    fontSize:10,
                    color:const Color.fromRGBO(0,0,0,0.5),
                  ),
                ),
              ),
            ),
            /// Transaction Container
            const SizedBox(
              height: 11,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _flag1 = !_flag1;
                });
              },
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context)=> const TransactionScreen()),
                  );
                },
                child: Container(
                  height: 40,
                  width: 184,
                  decoration:  BoxDecoration(
                    borderRadius:const BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color:_flag1? Colors.transparent: const Color.fromRGBO(14, 161, 125, 0.15),
                  ),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: SvgPicture.asset(
                          "assets/icons/Subtract.svg",
                          height: 17,
                          width: 18,
                          color:_flag1? const Color.fromRGBO(14, 161, 125, 1): const Color.fromRGBO(0,0,0, 1),
                        ),
                      ),
                      const SizedBox(
                        width: 6,
                      ),
                      Text("Transaction",
                        style: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:16,
                          color:const Color.fromRGBO(0,0,0, 1),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            /// GRAPH CONTAINER
            const SizedBox(
              height: 11,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _flag2 = !_flag2;
                });
              },
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context, 
                    MaterialPageRoute(
                      builder: (context)=> const Graph(),
                    ),
                  );
                },
                child: Container(
                  height: 40,
                  width: 184,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color:_flag2?  Colors.transparent: const Color.fromRGBO(14, 161, 125, 0.15),
                  ),               
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: SvgPicture.asset(
                          "assets/icons/graph.svg",
                          height: 17,
                          width: 18,
                          color:_flag2? const Color.fromRGBO(14, 161, 125, 1): const Color.fromRGBO(0,0,0, 1),
                        ),
                      ),
                      const SizedBox(
                        width: 6,
                      ),
                      Text("Graphs",
                        style: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:16,
                          color:const Color.fromRGBO(0,0,0, 1),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            /// CATEGORY CONTAINER
            const SizedBox(
              height: 11,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _flag3 = !_flag3;
                });
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context)=> const Category(),
                  ),
                );
              },
              child: Container(
                height: 40,
                width: 184,
                decoration:  BoxDecoration(
                  borderRadius:const BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                  color:_flag3?  Colors.transparent: const Color.fromRGBO(14, 161, 125, 0.15),
                ),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: SvgPicture.asset(
                        "assets/icons/category.svg",
                        height: 17,
                        width: 18,
                        color:_flag3? const Color.fromRGBO(14, 161, 125, 1): const Color.fromRGBO(0,0,0, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 6,
                    ),
                    Text("Category",
                      style: GoogleFonts.poppins(
                        fontWeight:FontWeight.w400,
                        fontSize:16,
                        color:const Color.fromRGBO(0,0,0, 1),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            /// TRASH CONTAINER
            const SizedBox(
              height: 11,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _flag4 = !_flag4;
                });
              },
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context, 
                    MaterialPageRoute(
                      builder: (context)=> const TrashScreen(),
                    ),
                  );
                },
                child: Container(
                  height: 40,
                  width: 184,
                  decoration:  BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color:_flag4?  Colors.transparent: const Color.fromRGBO(14, 161, 125, 0.15),
                  ),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: SvgPicture.asset(
                          "assets/icons/trash.svg",
                          height: 17,
                          width: 18,
                          color:_flag4? const Color.fromRGBO(14, 161, 125, 1): const Color.fromRGBO(0,0,0, 1),
                        ),
                      ),
                      const SizedBox(
                        width: 6,
                      ),
                      Text("Trash",
                        style: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:16,
                          color:const Color.fromRGBO(0,0,0, 1),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            /// ABOUT CONTAINER
            const SizedBox(
              height: 11,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _flag5 = !_flag5;
                });
              },
              child: Container(
                height: 40,
                width: 184,
                decoration:  BoxDecoration(
                  borderRadius:const BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                  color:_flag5?  Colors.transparent : const Color.fromRGBO(14, 161, 125, 0.15),
                ),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: SvgPicture.asset(
                        "assets/icons/about.svg",
                        height: 17,
                        width: 18,
                        color:_flag5? const Color.fromRGBO(14, 161, 125, 1): const Color.fromRGBO(0,0,0, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 6,
                    ),
                    Text("About us",
                      style: GoogleFonts.poppins(
                        fontWeight:FontWeight.w400,
                        fontSize:16,
                        color:const Color.fromRGBO(0,0,0, 1),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
  }
}