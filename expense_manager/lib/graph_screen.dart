
import 'package:expense_manager/drawer_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pie_chart/pie_chart.dart';

class Graph extends StatefulWidget {
  const Graph({super.key});

  @override
  State createState()=> _GraphState();
}

class Expenses {
  final String image;
  final String expenseName;
  final String money;

  const Expenses({
    required this.image,
    required this.expenseName,
    required this.money,
  });
}

class _GraphState extends State<Graph> {

  Map<String,double> data = {
    "Food":30,
    "Fuel":10,
    "Medicine":20,
    "Entertainment":20,
    "Shopping":15
  };

  List expenseList = [
    const Expenses(
      image: "assets/images/food.png", 
      expenseName: "Food", 
      money: "₹ 650.00",
    ),
    const Expenses(
      image: "assets/images/fuel.png", 
      expenseName: "Fuel", 
      money: "₹ 600.00",
    ),
    const Expenses(
      image: "assets/images/medicine.png", 
      expenseName: "Medicine", 
      money: "₹ 600.00",
    ),
    const Expenses(
      image: "assets/images/entertainment.png", 
      expenseName: "Entertainment", 
      money: "₹ 475.00",
    ),
    const Expenses(
      image: "assets/images/shopping.png", 
      expenseName: "Shopping", 
      money: "₹ 325.00",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyDrawer(),
      appBar: AppBar(
        title: Text("Graphs",
          style: GoogleFonts.poppins(
            fontWeight:FontWeight.w500,
            fontSize:16,
            color:const Color.fromRGBO(0,0,0,1),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(23.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            
            PieChart(
              chartValuesOptions: const ChartValuesOptions(showChartValues: false),
              dataMap: data,
              chartType: ChartType.ring,
              chartRadius: 177.08,
              ringStrokeWidth: 40,
              centerWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Total",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w500,
                      fontSize:10,
                      color:const Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                  Text("₹ 2550.00",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w500,
                      fontSize:13,
                      color:const Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            const Divider(
              thickness: 1,
              color: Color.fromRGBO(0, 0, 0, 0.5),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0,right: 20,top: 10),
              child: SizedBox(
                height: 280,
                width: double.infinity,
                child: ListView.builder(
                  itemCount: expenseList.length,
                  itemBuilder: (BuildContext context,int index){
                    return Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        children: [
                          Image.asset(
                            expenseList[index].image,
                            height: 40,
                            width: 40,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(expenseList[index].expenseName,
                            style: GoogleFonts.poppins(
                              fontWeight:FontWeight.w500,
                              fontSize:13,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          const Spacer(),
                          Text(expenseList[index].money,
                            style: GoogleFonts.poppins(
                              fontWeight:FontWeight.w500,
                              fontSize:13,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.arrow_forward_ios_rounded,
                              size: 10,  
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Divider(
              thickness: 1,
              color: Color.fromRGBO(0, 0, 0, 0.5),
            ),

            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text("Total",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:16,
                      color:const Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                  Text("₹ 2550.00",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w600,
                      fontSize:15,
                      color:const Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}