class GridCartModel {
  String? image;
  double? price;
  String? name;

  GridCartModel({
    this.image,
    this.name,
    this.price,
  });
}
