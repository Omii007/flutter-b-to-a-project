
// ignore_for_file: use_build_context_synchronously

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path ;



class ToDoApp extends StatefulWidget {
  const ToDoApp({super.key});

  @override
  State createState() => _ToDoAppState();
}

class ToDoModelClass{
  int? id;
  String title;
  String description;
  String date;
  bool flag1;

  ToDoModelClass({
    this.id,
    required this.title,
    required this.description,
    required this.date,
    this.flag1 = false,
  });

  Map<String,dynamic> todoMap(){
    return {
      'title': title,
      'description': description,
      'date': date,
      'flag1': flag1,
    };
  }

  @override
  String toString(){
    return '{id:$id, title: $title, description: $description, date: $date}';
  }
}

class _ToDoAppState extends State <ToDoApp> {

  @override
  void initState() {
    super.initState();
    initializedDatabase(); // DATABASE INITIALIZED START
    print("Activated Database");
  }
  dynamic database;

  // CARDLIST INITIALIZE BEFORE CALL FOR ACTIVEDATABASE 
  // IN ACTIVEDATABASE DATABASE CREATE
  Future initializedDatabase()async{
    cardList = await activeDatabase(); // COPY DATA
    setState(() {});
  }

  Future<List<ToDoModelClass>> activeDatabase()async{
    try{
      database = openDatabase(
    path.join(await getDatabasesPath(),"Advance.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute(
        '''CREATE TABLE ToDoDataBase(
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            description TEXT NOT NULL,
            date INT NOT NULL,
            flag1 BOOLEAN
          )
        '''
        );
      db.execute(
        '''
          CREATE TABLE RegisterDataBase(
            id INTEGER PRIMARY KEY,
            username TEXT PRIMARY KEY,
            password TEXT NOT NULL
          )
        '''
      );
      },
    );
    }catch(ie){
      print("Error opening database : $ie");
    }
    List<ToDoModelClass> initialList = await getData();
    return initialList;
    // await updateList();
    // print("List updated in activeDatabase");
  }

  // DATA INSERT INTO DATABASE
  Future<void> insertData(ToDoModelClass obj)async{
    final localDB = await database;

    await localDB.insert(
      "ToDoDataBase",
      obj.todoMap(),
      conflictAlgorithm:ConflictAlgorithm.replace,
    );
    cardList = await getData();
    setState(() {});
    print("Data inserted in database");
  }

  // FETCH OR RETRIVE DATA IN DATABASE

  Future<List<ToDoModelClass>> getData()async{
    final localDB = await database;

    List <Map<String,dynamic>> mapList = await localDB.query("ToDoDataBase");
    print("In fetch data");
    print(mapList);
    return List.generate(mapList.length, (index) {
      return ToDoModelClass(
        id: mapList[index]['id'],
        title: mapList[index]['title'], 
        description: mapList[index]['description'], 
        date: mapList[index]['date'],
        flag1: mapList[index]['flag1'] == 0 ? false:true,
      );
    });
  }

  // DELETE DATA IN DATABASE
  Future<void> deleteDataBaseCard(ToDoModelClass obj)async{
    final localDB = await database;

    await localDB.delete(
      "ToDoDataBase",
      where : "id = ?",
      whereArgs : [obj.id],
    );
    cardList = await getData();
    setState(() {});
  } 

  // UPDATE DATA IN DATABASE

  Future<void> updateDataBase(ToDoModelClass obj)async{
    final localDB = await database;
    print("${obj.flag1}");
    await localDB.update(
      "ToDoDataBase",
      obj.todoMap(),
      where: "id = ?",
      whereArgs: [obj.id],
    );
    cardList = await getData();
    setState(() {});
  }

  

  // Future<void> updateList()async{
  //   cardList = await getData();
  //   // print("--------- updated list ------------");
  //   print(cardList);                
    
  // }

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List cardList = [];
  int localId = 1;
  bool flag = false;

  // void showMarked(ToDoModelClass index){
  //   setState(() {
  //     cardList[index].flag1 = !cardList[index].flag1;
  //   });
  // }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 45,left: 29),
            child: SizedBox(
              height: 75,
              width: 370,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: SizedBox(
                          // height: 28,
                          width: 350,
                          child: Text("Good morning",
                            style: GoogleFonts.quicksand(
                              fontWeight:FontWeight.w400,
                              fontSize:22,
                              color:const Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text("Omkar",
                          style: GoogleFonts.quicksand(
                            fontWeight:FontWeight.w600,
                            fontSize:30,
                            color:const Color.fromRGBO(255, 255, 255, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              child: Container(
                width: double.infinity,
                margin: const EdgeInsets.only(top: 43),
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(217, 217, 217, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text("CREATE TO DO LIST",
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w500,
                          fontSize:12,
                          color:const Color.fromRGBO(0,0,0,1),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 20),
                        decoration: const BoxDecoration(
                         color: Color.fromRGBO(255, 255, 255, 1),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          ),
                        ),
                        child: ListView.builder(
                          itemCount: cardList.length,
                          itemBuilder: (BuildContext context,int index){
                            return  Slidable(
                                endActionPane:  ActionPane(
                                  extentRatio: 0.2,
                                  motion: const DrawerMotion(),
                                  children:  [
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [

                                          const SizedBox(
                                            height: 5,
                                          ),
                                          GestureDetector(
                                            onTap: () {  
                                              // INDEX PASS FOR EDIT DATA IN LIST                          
                                             editTask(cardList[index]);
                                            },
                                            child: Container(
                                              height: 32,
                                              width: 32,
                                              decoration: const BoxDecoration(
                                                color: Color.fromRGBO(89, 57, 241, 1),
                                                shape: BoxShape.circle,
                                                boxShadow: [
                                                  BoxShadow(
                                                    blurRadius: 10,
                                                    color: Color.fromRGBO(0, 0, 0, 0.1),
                                                  ),
                                                ],
                                              ),
                                              child: const Icon(
                                                Icons.edit_outlined,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                            // INDEX PASS TO DATABASE WHICH OBJECT DELETE IN DATABASE
                                              
                                              removeTask(cardList[index],);
                                            },
                                            child: Container(
                                              height: 32,
                                              width: 32,
                                              decoration: const BoxDecoration(
                                                color: Color.fromRGBO(89, 57, 241, 1),
                                                shape: BoxShape.circle,
                                                boxShadow: [
                                                  BoxShadow(
                                                    blurRadius: 10,
                                                    color: Color.fromRGBO(0, 0, 0, 0.1),
                                                  ),
                                                ],
                                              ),
                                              child: const Icon(Icons.delete_outline_outlined,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                         
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),  
                                  ],
                                ),
                                child:  Container(
                                      // margin: const EdgeInsets.only(top: 10),
                                      width: double.infinity,
                                      decoration: const BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            offset: Offset(0, 4),
                                            blurRadius: 20,
                                            color: Color.fromRGBO(0, 0, 0, 0.08),
                                          ),
                                        ],
                                        // border: Border.all(color: Colors.black)
                                      ),
                                      
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(left: 16,top: 10),
                                            child: Container(
                                              height: 52,
                                              width: 52,
                                              decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color.fromRGBO(217, 217, 217, 1),
                                              ),
                                              child: Image.asset(
                                                "assets/images/login.png",
                                                fit: BoxFit.fitHeight,
                                              ),
                                            ),
                                          ), 
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 20,top: 12),
                                                      child: SizedBox(
                                                        child:  Text(cardList[index].title,
                                                            style: GoogleFonts.inter(
                                                              fontSize:11,
                                                              fontWeight:FontWeight.w500,
                                                              color:const Color.fromRGBO(0, 0, 0, 1),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                            
                                                  ],
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 20,top: 8),
                                                  child: SizedBox(
                                                      // height: 30,
                                                      width: 270,
                                                      child: Text(cardList[index].description,
                                                        style: GoogleFonts.inter(
                                                          fontSize:9,
                                                          fontWeight:FontWeight.w400,
                                                          color:const Color.fromRGBO(0, 0, 0, 0.7),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                
                                                Padding(
                                              padding: const EdgeInsets.only(left: 20,top: 8),
                                              child:  SizedBox(
                                                  width: 200,
                                                  child: Text(cardList[index].date,
                                                    style: GoogleFonts.inter(
                                                      fontSize:11,
                                                      fontWeight:FontWeight.w400,
                                                      color:const Color.fromRGBO(0, 0, 0, 0.7),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            
                                            const SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                      ),
                                      // const SizedBox(
                                      //   width: 10,
                                      // ),
                                      Padding(
                                        padding: const EdgeInsets.only(right: 15),
                                        child: SizedBox(
                                          height: 15,
                                          width: 15,
                                          child: GestureDetector(
                                            
                                            onTap: (){
                                              setState(() {
                                                cardList[index].flag1 = !cardList[index].flag1;
                                                updateDataBase(cardList[index]);
                                              });
                                            },
                                            child: 
                                              cardList[index].flag1? Image.asset("assets/icons/true.png",
                                                ) : 
                                                Image.asset("assets/icons/false.png",
                                                ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                
                              
                            );
                        
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),     
      ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            titleController.clear();
            descriptionController.clear();
            dateController.clear();
            isBottomSheet(false);
          });
        },
        child: const Icon(Icons.add,
          size: 40,
        ),  
      ),
    );
  }

  void editTask(ToDoModelClass toDoModelObj){
    // ASSIGN THE TEXT EDITING CONTROLLERS WITH THE TEXT VALUES AND THEN OPEN THE BOTTOM SHEET
    titleController.text = toDoModelObj.title;
    descriptionController.text = toDoModelObj.description;
    dateController.text = toDoModelObj.date;
    print(toDoModelObj);
    isBottomSheet(true,toDoModelObj);
  }

  // REMOVE CARD IN THE LIST
  Future<void> removeTask(ToDoModelClass obj)async{

    await deleteDataBaseCard(obj);
    setState(() {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Card delete successful"),
          backgroundColor: Color.fromARGB(255, 76, 149, 175),
        ),
      );  
    });
  }

  @override
  void dispose(){
    super.dispose();
    titleController.dispose();
    descriptionController.dispose();
    dateController.dispose();
  }

  // WHEN WITHOUT FILL THE DATA SUBMIT BUTTON CLICK THEN THIS CODE WORK
  void onSubmit(bool flag,[ToDoModelClass? editIndex ])async{

    bool enterData = _formKey.currentState!.validate();
    if(titleController.text.trim().isNotEmpty 
        && descriptionController.text.trim().isNotEmpty 
        && dateController.text.trim().isNotEmpty && enterData){
        if(!flag){
          setState(()async{
            await insertData(
              ToDoModelClass(                
                title: titleController.text.trim(), 
                description: descriptionController.text.trim(), 
                date: dateController.text.trim(),
                flag1: false,
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("Data add successful"),
                backgroundColor: Colors.green,
              ),
            );  
          });                 
                 
        }else{
          setState(() async{
            print("In updateddatbase");
            await updateDataBase(
              ToDoModelClass(
                id: editIndex!.id,
                title: titleController.text.trim(),
                description: descriptionController.text.trim(),
                date: dateController.text.trim(),
                flag1: true,
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("Data updated successful"),
                backgroundColor: Colors.green,
              ),
            ); 
          });       
        }
    } 
    clearController(); 
  }

  // TO CLEAR ALL THE TEXT EDITING CONTROLLERS

  void clearController(){
    titleController.clear();
    descriptionController.clear();
    dateController.clear();
  }

  void isBottomSheet(bool flag,[ToDoModelClass? editIndex ]){
    
      showModalBottomSheet(
      isScrollControlled: true,
      context: context, 
      builder: (context) {
        return Form(
          key: _formKey,
          child: Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Text("Create Task",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      color: const Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Text("Title",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: const Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          controller: titleController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            hintText: "Enter a title",
                            label: const Text("Enter a title"),
                            hintStyle: GoogleFonts.quicksand(
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                              color: const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          validator: (value) {
                            if(value == null || value.trim().isEmpty){
                              return "Please enter title";
                            }else{
                              return null;
                            }
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text("Description",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: const Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          controller: descriptionController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            hintText: "Enter a description",
                            label: const Text("Enter a description"),
                            hintStyle: GoogleFonts.quicksand(
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                              color: const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          validator: (value) {
                            if(value == null || value.trim().isEmpty){
                              return "Please enter title";
                            }else{
                              return null;
                            }
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text("Date",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: const Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          controller: dateController,
                          decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month_outlined,size: 25,),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            hintText: "Enter a date",
                            label: const Text("Enter a date"),
                            hintStyle: GoogleFonts.quicksand(
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                              color: const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          readOnly: true,
                          onTap: ()async {
                            // pick the Date from datepicker
                            
                            DateTime? pickDate = await showDatePicker(
                              context: context, 
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2024), 
                              lastDate: DateTime(2025),
                    
                            );
                            String formatedDate = DateFormat.yMMMd().format(pickDate!);
                    
                            setState(() {
                              dateController.text = formatedDate;
                            });
                          },
                          validator: (value) {
                            if(value == null || value.trim().isEmpty){
                              return "please select the date";
                            }else{
                              return null;
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 50,
                    width: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      boxShadow: const[
                        BoxShadow(
                          color: Color.fromRGBO(0,0,0,0.1),
                          blurRadius: 10,
                          spreadRadius: 1,
                        ),
                      ],
                    ),
                    child: ElevatedButton(
                      onPressed: ()async{
                        
                        flag? onSubmit(flag,editIndex): onSubmit(flag);                 
                        Navigator.of(context).pop();
                      }, 
                      style: const ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(Color.fromRGBO(0,139,148,1),),
                      ),
                      child: Text("Submit",
                        style: GoogleFonts.inter(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );

  }

  
}

