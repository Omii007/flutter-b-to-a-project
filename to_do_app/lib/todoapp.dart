
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ToDoApp extends StatefulWidget {
  const ToDoApp({super.key});

  @override
  State createState() => _ToDoAppState();
}

class ToDoModelClass{
  String title;
  String description;
  String date;
  bool flag1;

  ToDoModelClass({
    required this.title,
    required this.description,
    required this.date,
    this.flag1 = false,
  });
}

class _ToDoAppState extends State <ToDoApp> {

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<ToDoModelClass> cardList = [];
  int count = 1;
  bool flag = false;

  // void showMarked(ToDoModelClass index){
  //   setState(() {
  //     cardList[index].flag1 = !cardList[index].flag1;
  //   });
  // }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 45,left: 29),
            child: SizedBox(
              height: 75,
              width: 370,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: SizedBox(
                          // height: 28,
                          width: 350,
                          child: Text("Good morning",
                            style: GoogleFonts.quicksand(
                              fontWeight:FontWeight.w400,
                              fontSize:22,
                              color:const Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text("Omkar",
                          style: GoogleFonts.quicksand(
                            fontWeight:FontWeight.w600,
                            fontSize:30,
                            color:const Color.fromRGBO(255, 255, 255, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              child: Container(
                width: double.infinity,
                margin: const EdgeInsets.only(top: 43),
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(255,255,255, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text("CREATE TO DO LIST",
                        style: GoogleFonts.quicksand(
                          fontWeight: FontWeight.w500,
                          fontSize:12,
                          color:const Color.fromRGBO(0,0,0,1),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 20),
                        decoration: const BoxDecoration(
                         color: Color.fromRGBO(255, 255, 255, 1),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          ),
                        ),
                        child: ListView.builder(
                          itemCount: cardList.length,
                          itemBuilder: (BuildContext context,int index){
                            return  Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Slidable(
                                endActionPane:  ActionPane(
                                  motion:  const ScrollMotion(), 
                                  children:  [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          height: 32,
                                          width: 32,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            boxShadow: [
                                              BoxShadow(
                                                blurRadius: 10,
                                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                              ),
                                            ],
                                          ),
                                          child: SlidableAction(
                                            onPressed: (context) {
                                              setState(() {  
                                                editTask(cardList[index]);
                                              });
                                            },
                                            borderRadius: const BorderRadius.all(Radius.circular(50)),
                                            icon: Icons.edit_outlined,
                                            padding: const EdgeInsets.all(5),
                                            backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
                                            foregroundColor: const Color.fromRGBO(255, 255, 255, 1),
                                            ),
                                          ),
                                        Container(
                                          height: 32,
                                          width: 32,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            boxShadow: [
                                              BoxShadow(
                                                blurRadius: 10,
                                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                              ),
                                            ],
                                          ),
                                          child: SlidableAction(
                                            onPressed: (context) {
                                              removeTask(cardList[index]);
                                            },
                                            borderRadius: const BorderRadius.all(Radius.circular(50)),
                                            icon: Icons.delete_outline_outlined,
                                            padding: const EdgeInsets.all(2),
                                            backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
                                            foregroundColor: const Color.fromRGBO(255, 255, 255, 1),
                                          ),
                                        ),
                                      ],
                                    ),  
                                  ],
                                ),
                                child:  Container(
                                      // margin: const EdgeInsets.only(top: 10),
                                      width: double.infinity,
                                      decoration: const BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            offset: Offset(0, 4),
                                            blurRadius: 20,
                                            color: Color.fromRGBO(0, 0, 0, 0.08),
                                          ),
                                        ],
                                        // border: Border.all(color: Colors.black)
                                      ),
                                      
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(left: 16,top: 10),
                                            child: Container(
                                              height: 52,
                                              width: 52,
                                              decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color.fromRGBO(217, 217, 217, 1),
                                              ),
                                              child: Image.asset(
                                                "assets/images/login.png",
                                                fit: BoxFit.fitHeight,
                                              ),
                                            ),
                                          ), 
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 20,top: 12),
                                                      child: SizedBox(
                                                        child:  Text(cardList[index].title,
                                                            style: GoogleFonts.inter(
                                                              fontSize:11,
                                                              fontWeight:FontWeight.w500,
                                                              color:const Color.fromRGBO(0, 0, 0, 1),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                            
                                                  ],
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 20,top: 8),
                                                  child: SizedBox(
                                                      // height: 30,
                                                      width: 270,
                                                      child: Text(cardList[index].description,
                                                        style: GoogleFonts.inter(
                                                          fontSize:9,
                                                          fontWeight:FontWeight.w400,
                                                          color:const Color.fromRGBO(0, 0, 0, 0.7),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                
                                                Padding(
                                              padding: const EdgeInsets.only(left: 20,top: 8),
                                              child:  SizedBox(
                                                  width: 200,
                                                  child: Text(cardList[index].date,
                                                    style: GoogleFonts.inter(
                                                      fontSize:11,
                                                      fontWeight:FontWeight.w400,
                                                      color:const Color.fromRGBO(0, 0, 0, 0.7),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            
                                            const SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                      ),
                                      // const SizedBox(
                                      //   width: 10,
                                      // ),
                                      Padding(
                                        padding: const EdgeInsets.only(right: 15),
                                        child: SizedBox(
                                          height: 15,
                                          width: 15,
                                          child: GestureDetector(
                                            
                                            onTap: (){
                                              setState(() {
                                                cardList[index].flag1 = !cardList[index].flag1;
                                              });
                                            },
                                            child: 
                                              cardList[index].flag1? Image.asset("assets/icons/true.png",
                                                ) : 
                                                Image.asset("assets/icons/false.png",
                                                ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                
                              ),
                            );
                        
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),     
      ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            titleController.clear();
            descriptionController.clear();
            dateController.clear();
            isBottomSheet(false);
          });
        },
        child: const Icon(Icons.add,
          size: 40,
        ),  
      ),
    );
  }

  void editTask(ToDoModelClass toDoModelClass){
    // ASSIGN THE TEXT EDITING CONTROLLERS WITH THE TEXT VALUES AND THEN OPEN THE BOTTOM SHEET
    titleController.text = toDoModelClass.title;
    descriptionController.text = toDoModelClass.description;
    dateController.text = toDoModelClass.date;

    isBottomSheet(true,toDoModelClass);
  }

  // TO CLEAR ALL THE TEXT EDITING CONTROLLERS

  void clearController(){
    titleController.clear();
    descriptionController.clear();
    dateController.clear();
  }

  // REMOVE CARD IN THE LIST
  void removeTask(ToDoModelClass index){
    setState(() {
      cardList.remove(index);
    });
  }

  @override
  void dispose(){
    super.dispose();
    titleController.dispose();
    descriptionController.dispose();
    dateController.dispose();
  }

  // WHEN WITHOUT FILL THE DATA SUBMIT BUTTON CLICK THEN THIS CODE WORK
  void onSubmit(bool flag,[ToDoModelClass? editIndex ]){

    bool enterData = _formKey.currentState!.validate();
    if(titleController.text.trim().isNotEmpty 
        && descriptionController.text.trim().isNotEmpty 
        && dateController.text.trim().isNotEmpty && enterData){
        if(!flag){
          setState(() {
            cardList.add(
              ToDoModelClass(
                title: titleController.text.trim(), 
                description: descriptionController.text.trim(), 
                date: dateController.text.trim(),
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("Data add successful"),
              backgroundColor: Colors.green,
              ),
            );
          });
          Navigator.of(context).pop();
          clearController();
        }else{
          setState(() {
          editIndex!.title = titleController.text.trim();
          editIndex.description = descriptionController.text.trim();
          editIndex.date = dateController.text.trim();
        });
        Navigator.of(context).pop();
        clearController();  
      }
    } 
     
  }

  void isBottomSheet(bool flag,[ToDoModelClass? editIndex ]){
    
      showModalBottomSheet(
      isScrollControlled: true,
      context: context, 
      builder: (context) {
        return Form(
          key: _formKey,
          child: Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Text("Create Task",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      color: const Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Text("Title",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: const Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          controller: titleController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            hintText: "Enter a title",
                            label: const Text("Enter a title"),
                            hintStyle: GoogleFonts.quicksand(
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                              color: const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          validator: (value) {
                            if(value == null || value.trim().isEmpty){
                              return "Please enter title";
                            }else{
                              return null;
                            }
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text("Description",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: const Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          controller: descriptionController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            hintText: "Enter a description",
                            label: const Text("Enter a description"),
                            hintStyle: GoogleFonts.quicksand(
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                              color: const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          validator: (value) {
                            if(value == null || value.trim().isEmpty){
                              return "Please enter title";
                            }else{
                              return null;
                            }
                          },
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text("Date",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: const Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextFormField(
                          controller: dateController,
                          decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month_outlined,size: 25,),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            hintText: "Enter a date",
                            label: const Text("Enter a date"),
                            hintStyle: GoogleFonts.quicksand(
                              fontWeight: FontWeight.normal,
                              fontSize: 15,
                              color: const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          readOnly: true,
                          onTap: ()async {
                            // pick the Date from datepicker
                    
                            DateTime? pickDate = await showDatePicker(
                              context: context, 
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2024), 
                              lastDate: DateTime(2025),
                    
                            );
                            String formatedDate = DateFormat.yMMMd().format(pickDate!);
                    
                            setState(() {
                              dateController.text = formatedDate;
                            });
                          },
                          validator: (value) {
                            if(value == null || value.trim().isEmpty){
                              return "please select the date";
                            }else{
                              return null;
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 50,
                    width: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      boxShadow: const[
                        BoxShadow(
                          color: Color.fromRGBO(0,0,0,0.1),
                          blurRadius: 10,
                          spreadRadius: 1,
                        ),
                      ],
                    ),
                    child: ElevatedButton(
                      onPressed: (){

                         flag? onSubmit(flag,editIndex): onSubmit(flag);
                      }, 
                      style: const ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(Color.fromRGBO(0,139,148,1),),
                      ),
                      child: Text("Submit",
                        style: GoogleFonts.inter(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        );
      },);
  }
}

