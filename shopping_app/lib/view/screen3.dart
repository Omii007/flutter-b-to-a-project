import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shopping_app/view/screen4.dart';

class Screen3 extends StatefulWidget {
  const Screen3({super.key});

  @override
  State<Screen3> createState() => _Screen3State();
}

class _Screen3State extends State<Screen3> {
  int selectedIndex = 0;
  List<String> sizeList = [
    "S",
    "M",
    "L",
    "XL",
    "XXL",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Details",
          style: GoogleFonts.imprima(
            fontWeight: FontWeight.w400,
            fontSize: 18,
            color: const Color.fromRGBO(13, 13, 13, 1),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(
            Icons.arrow_back_ios_new_rounded,
          ),
          iconSize: 20,
          color: const Color.fromRGBO(13, 13, 14, 1),
        ),
        actions: [
          Image.asset(
            "assets/icons/s_3_icon.png",
            height: 22,
            width: 20,
            color: const Color.fromRGBO(13, 13, 14, 1),
          ),
          const SizedBox(
            width: 30,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Container(
                height: MediaQuery.of(context).size.height / 2.1,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                ),
                child: Image.asset(
                  "assets/images/s_image_6.png",
                  filterQuality: FilterQuality.high,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Row(
                children: [
                  Text(
                    "Premium\nTagerine Shirt",
                    style: GoogleFonts.imprima(
                      fontWeight: FontWeight.w700,
                      fontSize: 30,
                      color: const Color.fromRGBO(13, 13, 13, 1),
                    ),
                  ),
                  const Spacer(),
                  Container(
                    height: 24,
                    width: 24,
                    decoration: const BoxDecoration(shape: BoxShape.circle),
                    child: Image.asset(
                      "assets/icons/icon_1.png",
                      filterQuality: FilterQuality.high,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 24,
                    width: 24,
                    decoration: const BoxDecoration(shape: BoxShape.circle),
                    child: Image.asset(
                      "assets/icons/icon_2.png",
                      filterQuality: FilterQuality.high,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 24,
                    width: 24,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset(
                      "assets/icons/icon_3.png",
                      filterQuality: FilterQuality.high,
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Text(
                "Size",
                style: GoogleFonts.imprima(
                  fontWeight: FontWeight.w700,
                  fontSize: 24,
                  color: const Color.fromRGBO(13, 13, 13, 1),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: SizedBox(
                height: 44,
                child: ListView.builder(
                  itemExtent: 70,
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: sizeList.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedIndex = index;
                        });
                      },
                      child: Container(
                        height: 44,
                        width: 45,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: selectedIndex == index
                              ? const Color.fromRGBO(13, 13, 14, 1)
                              : Colors.transparent,
                        ),
                        child: Center(
                          child: Text(
                            sizeList[index],
                            style: GoogleFonts.imprima(
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                              color: selectedIndex == index
                                  ? const Color.fromRGBO(255, 255, 255, 1)
                                  : const Color.fromRGBO(121, 119, 128, 1),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            const SizedBox(
              height: 28,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  "\$257.85",
                  style: GoogleFonts.imprima(
                    fontWeight: FontWeight.w700,
                    fontSize: 36,
                    color: const Color.fromRGBO(13, 13, 13, 1),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Screen4(),
                      ),
                    );
                  },
                  child: Container(
                    height: 62,
                    width: MediaQuery.of(context).size.width / 2.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(90),
                      color: const Color.fromRGBO(255, 122, 0, 1),
                    ),
                    child: Center(
                      child: Text(
                        "Add To Cart",
                        style: GoogleFonts.imprima(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                          color: const Color.fromRGBO(255, 255, 255, 1),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
