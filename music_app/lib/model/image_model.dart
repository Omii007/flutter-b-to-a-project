class SongImage {
  String? image;
  String? name;
  int? year;

  SongImage({
    this.image,
    this.name,
    this.year,
  });
}
