import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:house_rental/screen1.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height / 1.3,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              "assets/images/home_1.png",
              fit: BoxFit.cover,
              filterQuality: FilterQuality.high,
            ),
          ),
          const SizedBox(
            height: 22,
          ),
          Text(
            "Lets find your Paradise",
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              color: const Color.fromRGBO(0, 0, 0, 1),
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          SizedBox(
            width: 232,
            child: Text(
              "Find your perfect dream space with just a few clicks",
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w400,
                fontSize: 15,
                color: const Color.fromRGBO(101, 101, 101, 1),
              ),
            ),
          ),
          const SizedBox(
            height: 13,
          ),
          SizedBox(
            height: 55,
            width: 220,
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Screen1(),
                  ),
                );
              },
              style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(
                  Color.fromRGBO(32, 169, 247, 1),
                ),
              ),
              child: Text(
                "Get Started",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 22,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
