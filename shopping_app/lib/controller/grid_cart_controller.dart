import 'package:flutter/material.dart';

import '../model/grid_cart_model.dart';

class GridCartController with ChangeNotifier {
  List<GridCartModel> gridList = [
    GridCartModel(
      image: "assets/images/s_image_2.png",
      price: 240.32,
      name: "Tagerine Shirt",
    ),
    GridCartModel(
      image: "assets/images/s_image_3.png",
      price: 325.36,
      name: "Leather Coart",
    ),
    GridCartModel(
        image: "assets/images/s_image_4.png",
        price: 126.47,
        name: "Tagerine Shirt"),
    GridCartModel(
      image: "assets/images/s_image_5.png",
      price: 257.85,
      name: "Leather Coart",
    ),
  ];
}
