import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shopping_app/view/screen1.dart';
import 'package:shopping_app/view/screen3.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 1.6,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32),
                border: null,
                image: const DecorationImage(
                  image: AssetImage(
                    "assets/images/s_image_1.png",
                  ),
                  filterQuality: FilterQuality.high,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 290,
              child: Text(
                "Find The \nBest Collections",
                overflow: TextOverflow.clip,
                style: GoogleFonts.imprima(
                  fontWeight: FontWeight.w800,
                  fontSize: 42,
                  color: const Color.fromRGBO(13, 13, 14, 1),
                ),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            SizedBox(
              width: 301,
              child: Text(
                "Get your dream item easily with FashionHub and get other intersting offer",
                overflow: TextOverflow.clip,
                style: GoogleFonts.imprima(
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                  color: const Color.fromRGBO(121, 119, 128, 1),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Screen3(),
                      ),
                    );
                  },
                  child: Container(
                    height: 62,
                    width: MediaQuery.of(context).size.width / 2.5,
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(90),
                      border: Border.all(
                        width: 1,
                        color: const Color.fromRGBO(13, 13, 13, 1),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Sign Up",
                        style: GoogleFonts.imprima(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                          color: const Color.fromRGBO(13, 13, 13, 1),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Screen1(),
                      ),
                    );
                  },
                  child: Container(
                    height: 62,
                    width: MediaQuery.of(context).size.width / 2.5,
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(255, 122, 0, 1),
                      borderRadius: BorderRadius.circular(90),
                      border: Border.all(
                        width: 1,
                        color: const Color.fromRGBO(255, 122, 0, 1),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Sign In",
                        style: GoogleFonts.imprima(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                          color: const Color.fromRGBO(255, 255, 255, 1),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
