import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class QuizScreen extends StatefulWidget {
  const QuizScreen({super.key});

  @override
  State createState() => _QuizScreenState();
}

class QuizModel {
  String? question;
  List<String>? options;
  int? answerIndex;
  String? explanation;

  QuizModel({this.question, this.options, this.answerIndex, this.explanation});
}

class _QuizScreenState extends State<QuizScreen> {
  int currentIndex = 0;
  int index = 0;

  List<QuizModel> questionList = [
    QuizModel(
      question:
          "Which data type can hold the value of both int and double in dart?",
      options: ["numeric", "int", "double", "num"],
      explanation:
          "The Dart Number (num) is used to store the numeric values. The number can be two types - int and double.",
      answerIndex: 3,
    ),
    QuizModel(
      question: "Which of the following data can be used to represent 'John'?",
      options: ["string", "char", "String", "str"],
      explanation:
          "String - A String is the sequence of the character. If we store the data like - name, address, special character, etc. It is signified by using either single quotes or double quotes.",
      answerIndex: 2,
    ),
    QuizModel(
      question:
          "To declare a variable in Dart, without specifying its data type you can use the keyword _____?",
      options: ["val", "var", "let", "num"],
      explanation:
          "var - In Dart, the var keyword is used for declaring variables when you want the Dart compiler to infer the data type of the variable based on the assigned value.",
      answerIndex: 1,
    ),
    QuizModel(
      question: "_____ data type can be used to represent true or false?",
      options: ["Boolean", "bool", "int", "num"],
      explanation:
          '" bool " is a built-in data type to represent the boolean data type.',
      answerIndex: 1,
    ),
    QuizModel(
      question: "Which of the following is not an Arithmetic operator?",
      options: ["-", "%", "/", "?"],
      explanation:
          "Among all the given options *\$ is not an Arithmetic operator. It is a special character in programming*. The *arithmetic operators* perform addition, subtraction, multiplication, division, exponentiation, and modulus operations.\n The *special symbols* have some special meaning and they cannot be used for other purposes.\n Some of the special symbols that are used in programming are as follows - \n [] {} (), ; = #",
      answerIndex: 3,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
        centerTitle: true,
        title: Text(
          "Math Quiz",
          style: GoogleFonts.dmSans(
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: const Color.fromRGBO(131, 76, 52, 1),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 29.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            LinearProgressIndicator(
              value: currentIndex + 1 / questionList.length,
              borderRadius: BorderRadius.circular(6),
              color: const Color.fromRGBO(42, 135, 63, 1),
            ),
            Row(
              children: [
                Text(
                  "${currentIndex + 1}",
                  style: GoogleFonts.dmSans(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: const Color.fromRGBO(0, 0, 0, 1),
                  ),
                ),
                Text(
                  "/${questionList.length}",
                  style: GoogleFonts.dmSans(
                    fontWeight: FontWeight.w400,
                    fontSize: 13,
                    color: const Color.fromRGBO(0, 0, 0, 1),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    decoration: const BoxDecoration(border: null),
                    child: Text(
                      "${questionList[index].question}",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.dmSans(
                        fontWeight: FontWeight.w700,
                        fontSize: 26,
                        color: const Color.fromRGBO(131, 76, 52, 1),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 17,
                  ),
                  GestureDetector(
                    onTap: () {
                      /// ADD LOGIC
                    },
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 145, 87, 1),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 25.0),
                              child: Text(
                                "${questionList[index].options?[0]}",
                                style: GoogleFonts.dmSans(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20,
                                  color: const Color.fromRGBO(255, 255, 255, 1),
                                ),
                              ),
                            ),
                          ),
                          const Spacer(),
                          Container(
                            height: 9.5,
                            width: 14,
                            margin: const EdgeInsets.only(right: 25),
                            child: const Icon(
                              Icons.arrow_forward_ios_rounded,
                              size: 15,
                              color: Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  /// 2nd OPTION
                  const SizedBox(
                    height: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      /// ADD LOGIC
                    },
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 145, 87, 1),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 25.0),
                              child: Text(
                                "${questionList[index].options?[1]}",
                                style: GoogleFonts.dmSans(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20,
                                  color: const Color.fromRGBO(255, 255, 255, 1),
                                ),
                              ),
                            ),
                          ),
                          const Spacer(),
                          Container(
                            height: 9.5,
                            width: 14,
                            margin: const EdgeInsets.only(right: 25),
                            child: const Icon(
                              Icons.arrow_forward_ios_rounded,
                              size: 15,
                              color: Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  /// 3RD OPTION
                  const SizedBox(
                    height: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      /// ADD LOGIC
                    },
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 145, 87, 1),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 25.0),
                              child: Text(
                                "${questionList[index].options?[2]}",
                                style: GoogleFonts.dmSans(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20,
                                  color: const Color.fromRGBO(255, 255, 255, 1),
                                ),
                              ),
                            ),
                          ),
                          const Spacer(),
                          Container(
                            height: 9.5,
                            width: 14,
                            margin: const EdgeInsets.only(right: 25),
                            child: const Icon(
                              Icons.arrow_forward_ios_rounded,
                              size: 15,
                              color: Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  /// 4TH OPTION
                  const SizedBox(
                    height: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      /// ADD LOGIC
                    },
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 145, 87, 1),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 25.0),
                              child: Text(
                                "${questionList[index].options?[3]}",
                                style: GoogleFonts.dmSans(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20,
                                  color: const Color.fromRGBO(255, 255, 255, 1),
                                ),
                              ),
                            ),
                          ),
                          const Spacer(),
                          Container(
                            height: 9.5,
                            width: 14,
                            margin: const EdgeInsets.only(right: 25),
                            child: const Icon(
                              Icons.arrow_forward_ios_rounded,
                              size: 15,
                              color: Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 26,
                  ),
                  Text(
                    "Explanation",
                    style: GoogleFonts.dmSans(
                      fontWeight: FontWeight.w700,
                      fontSize: 16,
                      color: const Color.fromRGBO(131, 76, 52, 1),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "${questionList[index].explanation}",
                      textAlign: TextAlign.start,
                      style: GoogleFonts.dmSans(
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: const Color.fromRGBO(131, 76, 52, 0.8),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
              width: 87,
              child: FloatingActionButton(
                onPressed: () {},
                backgroundColor: const Color.fromRGBO(26, 181, 134, 1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "NEXT",
                      style: GoogleFonts.dmSans(
                        fontWeight: FontWeight.w700,
                        fontSize: 15,
                        color: const Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    const Icon(
                      Icons.arrow_circle_right_rounded,
                      color: Color.fromRGBO(255, 255, 255, 1),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
