import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State createState() => _HomePageState();
}

class SubjectModel {
  String? firstName;
  String? subjectName;
  Color? color;

  SubjectModel({
    this.firstName,
    this.subjectName,
    this.color,
  });
}

class _HomePageState extends State<HomePage> {
  List<SubjectModel> subjectlist = [
    SubjectModel(
      firstName: "M",
      subjectName: "Math",
      color: const Color.fromRGBO(200, 60, 0, 1),
    ),
    SubjectModel(
      firstName: "H",
      subjectName: "History",
      color: const Color.fromRGBO(255, 157, 66, 1),
    ),
    SubjectModel(
      firstName: "G",
      subjectName: "Geography",
      color: const Color.fromRGBO(3, 163, 134, 1),
    ),
    SubjectModel(
      firstName: "B",
      subjectName: "Biology",
      color: const Color.fromRGBO(251, 43, 255, 1),
    ),
    SubjectModel(
      firstName: "S",
      subjectName: "Sports",
      color: const Color.fromRGBO(45, 104, 255, 1),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 27.0),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 59,
                    ),
                    Text(
                      "Hi Omkar,",
                      style: GoogleFonts.dmSans(
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: const Color.fromRGBO(131, 76, 52, 1),
                      ),
                    ),
                    Text(
                      "Great to see you again!",
                      style: GoogleFonts.dmSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(131, 76, 52, 1),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(top: 52.0),
                  child: Container(
                    height: 64,
                    width: 64,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromRGBO(250, 188, 154, 1)),
                  ),
                ),
              ],
            ),
            Expanded(
              child: ListView.builder(
                itemCount: subjectlist.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        /// ADD LOGIC TO RENDER NEXT SCREEN
                      },
                      child: Container(
                        height: 80,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: const Color.fromRGBO(255, 237, 217, 1),
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 22, right: 14),
                              child: Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color: const Color.fromRGBO(255, 255, 255, 1),
                                ),
                                child: Center(
                                  child: Text(
                                    "${subjectlist[index].firstName}",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.dmSans(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20,
                                      color: subjectlist[index].color,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Text(
                              "${subjectlist[index].subjectName}",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.dmSans(
                                fontWeight: FontWeight.w700,
                                fontSize: 20,
                                color: const Color.fromRGBO(131, 76, 52, 1),
                              ),
                            ),
                            const Spacer(),
                            Container(
                              height: 15,
                              width: 8,
                              margin: const EdgeInsets.only(right: 25),
                              child: const Icon(
                                Icons.arrow_forward_ios_rounded,
                                size: 15,
                                color: Color.fromRGBO(0, 0, 0, 1),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
