
import 'package:expense_manager/drawer_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TransactionScreen extends StatefulWidget {

  const TransactionScreen({super.key});

  @override
  State createState()=> _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /// DRAWER FOR MORE OPTIONS
      drawer: const MyDrawer(),
      appBar: AppBar(
        titleSpacing: 22,
        title: Text("April 2024",
          style: GoogleFonts.poppins(
            fontWeight:FontWeight.w500,
            fontSize:16,
            color:const Color.fromRGBO(33,33,33,1),
          ),
        ),
        actions: [
          IconButton(
            onPressed: (){}, 
            icon: const Icon(Icons.search_rounded,size: 24,),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: 3,
        itemBuilder: (BuildContext context,int index){
          return Column(
            children: [
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    const SizedBox(
                      width: 12,
                    ),
                    Column(
                      children: [                   
                        SizedBox(
                          height: 41,
                          width: 41,
                          child: Image.asset(
                            "assets/images/Mask group.png",
                            fit: BoxFit.cover,
                            filterQuality: FilterQuality.high,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 17,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                child: Text("Medicine",
                                  style: GoogleFonts.poppins(
                                    fontWeight:FontWeight.w400,
                                    fontSize:15,
                                    color:const Color.fromRGBO(0,0,0,1),
                                  ),
                                ),
                              ),
                              const Spacer(),
                              IconButton(
                                onPressed: (){},                          
                                  icon: const Icon(Icons.remove_circle_rounded,
                                  size: 15,
                                  color: Color.fromRGBO(246, 113, 49, 1),
                                ),
                              ),
                              Text("500",                          
                                style: GoogleFonts.poppins(
                                  fontWeight:FontWeight.w400,
                                  fontSize:15,
                                  color:const Color.fromRGBO(0,0,0,1),
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 251,
                            child: Text("Lorem Ipsum is simply dummy text of the ",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                          ),

                          Row(
                          children: [
                            const Spacer(),
                            Text("11 April",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                            Text(" | ",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                            Text("11:40 AM",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w400,
                                fontSize:10,
                                color:const Color.fromRGBO(0,0,0,0.8),
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        ],
                      ),
                    ),
                    
                  ],
                ),
              ),
              const Divider(
                indent: 10,
                height: 10,
                thickness: 1,
                color: Color.fromRGBO(206, 206, 206, 1),
              ),
            ],
          );
        },
      ),


      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterFloat,
      floatingActionButton: Container(
        height: 46,
        width: 166,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(67),
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 4,
              color:Color.fromRGBO(0,0,0,0.25),
            ),
          ],
        ),
        child: FloatingActionButton(
          onPressed: (){
            bottomSheet();
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(67),
          ),
          backgroundColor: const Color.fromRGBO(255,255,255,1),
          child: Row(
            children: [
              const SizedBox(
                width: 5,
              ),
              const Icon(Icons.add_circle_rounded,
                size: 32,
                color: Color.fromRGBO(14, 161, 125, 1),
              ),
              const SizedBox(
                width: 9,
              ),
              Text("Add Transaction",
                style: GoogleFonts.poppins(
                  fontWeight:FontWeight.w400,
                  fontSize:12,
                  color:const Color.fromRGBO(37,37,37,1),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void bottomSheet(){
    showModalBottomSheet(
      isScrollControlled: true,
      context: context, 
      backgroundColor: const Color.fromRGBO(255,255,255,1),
      builder: (context) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(22),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Date",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  // Date TextFormField
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(191, 189, 189, 1)
                          ),
                        ),
                        hintText: "Enter date",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:13,
                          color:const Color.fromRGBO(0,0,0,0.8),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 19,
                  ),
              
                  // Amount
                  Text("Amount",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  // Amount TextFormField
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(191, 189, 189, 1)
                          ),
                        ),
                        hintText: "Enter amount",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:13,
                          color:const Color.fromRGBO(0,0,0,0.8),
                        ),
                      ),
                    ),
                  ),
              
                  const SizedBox(
                    height: 19,
                  ),
              
                  // Category
                  Text("Category",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  // Category TextFormField
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(191, 189, 189, 1)
                          ),
                        ),
                        hintText: "Enter category",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:13,
                          color:const Color.fromRGBO(0,0,0,0.8),
                        ),
                      ),
                    ),
                  ),
              
                  // Description
                  const SizedBox(
                    height: 19,
                  ),
              
                  // Amount
                  Text("Description",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(33,33,33,1),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  // Amount TextFormField
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(191, 189, 189, 1)
                          ),
                        ),
                        hintText: "Enter description",
                        hintStyle: GoogleFonts.poppins(
                          fontWeight:FontWeight.w400,
                          fontSize:13,
                          color:const Color.fromRGBO(0,0,0,0.8),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 34,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          
                        },
                        child: Container(
                          height: 40,
                          width: 123,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(67),
                            color: const Color.fromRGBO(14, 161, 125, 1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(1, 2),
                                blurRadius: 2,
                                color: Color.fromRGBO(0,0,0,0.2),
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text("Add",
                              style: GoogleFonts.poppins(
                                fontWeight:FontWeight.w500,
                                fontSize:16,
                                color:const Color.fromRGBO(255,255,255,1),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

