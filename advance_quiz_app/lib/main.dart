import 'package:advance_quiz_app/views/quiz_screen.dart';
import 'package:advance_quiz_app/views/result_screen.dart';
import 'package:advance_quiz_app/views/splash_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ResultScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
