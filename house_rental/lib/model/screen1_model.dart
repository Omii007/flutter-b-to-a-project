class HomeInfoModel {
  String? image;
  String? name;
  String? place;
  int? price;

  HomeInfoModel({
    this.image,
    this.name,
    this.place,
    this.price,
  });
}
