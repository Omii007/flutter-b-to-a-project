import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shopping_app/controller/cart_controller.dart';

import 'screen5.dart';

class Screen4 extends StatefulWidget {
  const Screen4({super.key});

  @override
  State<Screen4> createState() => _Screen4State();
}

class _Screen4State extends State<Screen4> {
  int quantity = 1;

  @override
  Widget build(BuildContext context) {
    var cartList = Provider.of<CartController>(context).cartList;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Cart",
          style: GoogleFonts.imprima(
            fontWeight: FontWeight.w400,
            fontSize: 18,
            color: const Color.fromRGBO(13, 13, 13, 1),
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(
            Icons.arrow_back_ios_new_rounded,
          ),
          iconSize: 20,
          color: const Color.fromRGBO(13, 13, 14, 1),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 25,
                  ),
                  Text(
                    "My Orders",
                    style: GoogleFonts.imprima(
                      fontWeight: FontWeight.w700,
                      fontSize: 40,
                      color: const Color.fromRGBO(13, 13, 13, 1),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 2.5,
                      width: MediaQuery.of(context).size.width,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: cartList.length,
                        itemBuilder: (context, index) {
                          return Slidable(
                            key: ValueKey(index),
                            endActionPane: ActionPane(
                              extentRatio: 0.3,
                              motion: const ScrollMotion(),
                              openThreshold: 0.1,
                              children: [
                                Container(
                                  height: 55,
                                  width: 90,
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(30),
                                      topLeft: Radius.circular(30),
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SlidableAction(
                                        onPressed: (context) {},
                                        backgroundColor: const Color.fromRGBO(
                                            255, 122, 0, 1),
                                        icon: Icons.favorite_border_rounded,
                                        foregroundColor: const Color.fromRGBO(
                                            255, 255, 255, 1),
                                      ),
                                      SlidableAction(
                                        onPressed: (context) {},
                                        backgroundColor: const Color.fromRGBO(
                                            255, 122, 0, 1),
                                        icon: Icons.delete_outline,
                                        foregroundColor: const Color.fromRGBO(
                                            255, 255, 255, 1),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            direction: Axis.horizontal,
                            child: Container(
                              // height: 195,
                              margin: const EdgeInsets.only(bottom: 16.0),
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: null,
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 142,
                                    width: 108,
                                    child: Image.asset(
                                      cartList[index].image ?? '',
                                      filterQuality: FilterQuality.high,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 22,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            cartList[index].shirtName ?? '',
                                            overflow: TextOverflow.clip,
                                            style: GoogleFonts.imprima(
                                              fontWeight: FontWeight.w700,
                                              fontSize: 18,
                                              color: const Color.fromRGBO(
                                                  13, 13, 13, 1),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          Text(
                                            cartList[index].color ?? '',
                                            style: GoogleFonts.imprima(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: const Color.fromRGBO(
                                                  121, 119, 128, 1),
                                            ),
                                          ),
                                          Text(
                                            "Size ${cartList[index].size ?? ''}",
                                            style: GoogleFonts.imprima(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: const Color.fromRGBO(
                                                  121, 119, 128, 1),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 14,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                "\$${cartList[index].price ?? ''}",
                                                style: GoogleFonts.imprima(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 26,
                                                  color: const Color.fromRGBO(
                                                      13, 13, 13, 1),
                                                ),
                                              ),
                                              const Spacer(),
                                              TextButton.icon(
                                                onPressed: () {
                                                  setState(() {
                                                    cartList[index].quantity =
                                                        quantity++;
                                                  });
                                                },
                                                icon: const Icon(
                                                    Icons.close_rounded),
                                                label: Text(
                                                  "${cartList[index].quantity}",
                                                  style: GoogleFonts.imprima(
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 26,
                                                    color: const Color.fromRGBO(
                                                        13, 13, 13, 1),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  const Divider(
                    thickness: 1,
                    color: Color.fromRGBO(227, 227, 227, 1),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          text: "Total Items (3)",
                          style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(121, 119, 128, 1),
                          ),
                        ),
                      ),
                      const Spacer(),
                      Text.rich(
                        TextSpan(
                          text: "\$116.00",
                          style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(13, 13, 14, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          text: "Standard Delivery",
                          style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(121, 119, 128, 1),
                          ),
                        ),
                      ),
                      const Spacer(),
                      Text.rich(
                        TextSpan(
                          text: "\$12.00",
                          style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(13, 13, 14, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          text: "Total Payment",
                          style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(121, 119, 128, 1),
                          ),
                        ),
                      ),
                      const Spacer(),
                      Text.rich(
                        TextSpan(
                          text: "\$126.00",
                          style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(13, 13, 14, 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 35,
                  ),
                ],
              ),
            ),
            const Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Screen5(),
                      ),
                    );
                  },
                  child: Container(
                    height: 62,
                    width: MediaQuery.of(context).size.width / 2.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(90),
                      color: const Color.fromRGBO(255, 122, 0, 1),
                    ),
                    child: Center(
                      child: Text(
                        "Checkout Now",
                        style: GoogleFonts.imprima(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: const Color.fromRGBO(255, 255, 255, 1),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 3,
            ),
          ],
        ),
      ),
    );
  }
}
