import 'package:flutter/material.dart';

import '../model/screen1_model.dart';

class HomeInfoController with ChangeNotifier {
  List<HomeInfoModel> homeInfo = [
    HomeInfoModel(
      image: "assets/images/home_3.png",
      name: "Night Hill Villa",
      place: "London,Night Hill",
      price: 5900,
    ),
    HomeInfoModel(
      image: "assets/images/home_2.png",
      name: "Night Hill Villa",
      place: "London,New Park",
      price: 4900,
    ),
  ];
}
