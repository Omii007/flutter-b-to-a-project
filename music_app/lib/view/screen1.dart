import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/model/image_model.dart';
import 'package:music_app/model/song_model.dart';
import 'package:music_app/view/scree2.dart';

class Screen1 extends StatefulWidget {
  const Screen1({super.key});

  @override
  State<Screen1> createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  int selectedPageIndex = 0;

  List<String> imageList = ["1", "2", "3"];
  List<SongImage> songList = [
    SongImage(
        image: "assets/images/song1.png", name: "Dead inside", year: 2020),
    SongImage(image: "assets/images/song2.png", name: "Alone ", year: 2023),
    SongImage(image: "assets/images/song3.png", name: "Heartless ", year: 2023),
  ];

  List<SongModel> musicList = [
    SongModel(
        image: "assets/images/music1.png",
        songTitle: "We Are Chaos",
        year: 2023,
        subTitle: "Easy Living"),
    SongModel(
        image: "assets/images/music2.png",
        songTitle: "Smile ",
        specialKey: "*",
        year: 2023,
        subTitle: "Berrechid"),
  ];

  void selectPage(int index) {
    setState(() {
      selectedPageIndex = index;
    });
  }

  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(24, 24, 24, 1),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              children: [
                Stack(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 2,
                      width: double.infinity,
                      child: Image.asset(
                        "assets/images/screen1.png",
                        filterQuality: FilterQuality.high,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      top: 225,
                      left: 20,
                      child: SizedBox(
                        height: 44,
                        width: 170,
                        child: Text(
                          "A.L.O.N.E",
                          style: GoogleFonts.inter(
                            fontWeight: FontWeight.w600,
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 36,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 285,
                      left: 20,
                      child: Container(
                        height: 37,
                        width: 127,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(19),
                          color: const Color.fromRGBO(255, 46, 0, 1),
                        ),
                        child: Center(
                          child: Text(
                            "Subscribe",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              color: const Color.fromRGBO(19, 19, 19, 1),
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List<Widget>.generate(
                imageList.length,
                (index) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 3),
                  child: InkWell(
                    child: Container(
                      height: 7,
                      width: selectedIndex == index ? 21 : 7,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(22),
                        color: selectedIndex == index
                            ? const Color.fromRGBO(255, 61, 0, 1)
                            : const Color.fromRGBO(159, 159, 159, 1),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 17,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                children: [
                  Text(
                    "Discography",
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(255, 46, 0, 1),
                      fontSize: 16,
                    ),
                  ),
                  const Spacer(),
                  Text(
                    "See all",
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(248, 162, 69, 1),
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 19,
            ),
            SizedBox(
              // margin: const EdgeInsets.only(left: 15),
              height: 190,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: songList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Screen2(),
                          ),
                        );
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 140,
                            // width: 119,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Image.asset(
                                songList[index].image ?? '',
                                filterQuality: FilterQuality.high,
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "${songList[index].name}",
                            textAlign: TextAlign.start,
                            style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              color: const Color.fromRGBO(203, 200, 200, 1),
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            "${songList[index].year}",
                            style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              color: const Color.fromRGBO(203, 200, 200, 1),
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 17,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                children: [
                  Text(
                    "Popular singles",
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(255, 46, 0, 1),
                      fontSize: 16,
                    ),
                  ),
                  const Spacer(),
                  Text(
                    "See all",
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(248, 162, 69, 1),
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            ListView.builder(
              // clipBehavior: Clip.hardEdge,
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(horizontal: 1.0),
              physics: const NeverScrollableScrollPhysics(),
              itemCount: musicList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: Image.asset(
                    musicList[index].image ?? '',
                    filterQuality: FilterQuality.high,
                    fit: BoxFit.cover,
                  ),
                  title: Text(
                    "${musicList[index].songTitle}",
                    style: GoogleFonts.inter(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(203, 200, 200, 1),
                    ),
                  ),
                  subtitle: Text(
                    "${musicList[index].year} * ${musicList[index].subTitle}",
                    style: GoogleFonts.inter(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(132, 125, 125, 1),
                    ),
                  ),
                  trailing: PopupMenuButton(
                    iconColor: const Color.fromRGBO(217, 217, 217, 1),
                    itemBuilder: (context) {
                      return [];
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 24,
        selectedFontSize: 15,
        elevation: 0,
        onTap: selectPage,
        unselectedFontSize: 15,
        showUnselectedLabels: true,
        useLegacyColorScheme: false,
        unselectedItemColor: const Color.fromRGBO(157, 178, 206, 1),
        currentIndex: selectedPageIndex,
        selectedItemColor: const Color.fromRGBO(230, 154, 21, 1),
        items: const [
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(Icons.favorite_border_outlined),
            label: 'Favorite',
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(Icons.search_rounded),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(
              Icons.home_outlined,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(
              Icons.card_travel,
            ),
            label: "Cart",
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(19, 19, 19, 1),
            icon: Icon(
              Icons.person_3_outlined,
            ),
            label: "Profile",
          ),
        ],
      ),
    );
  }
}
