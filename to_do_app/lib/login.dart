import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:to_do_app/model.dart';
import 'package:to_do_app/register.dart';

// ignore: must_be_immutable
class Login extends StatefulWidget {
  Login({super.key,required this.registerModelClass});

  RegisterModelClass registerModelClass;

  @override
  State createState()=> _LoginState();
}

class _LoginState extends State<Login> {

  late RegisterModelClass registerModelClass;
  @override
  void initState() {
    super.initState();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool flag = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  image: DecorationImage(image: AssetImage(
                    "assets/images/todo1.jpg",
                    ),
                    fit: BoxFit.fill,
                    filterQuality: FilterQuality.high,
                  ),
                ),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 100,left: 10,right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Login",
                              style: GoogleFonts.inter(
                                fontWeight:FontWeight.bold,
                                fontSize:40,
                                color:Colors.black,
                              ),
                            ),
                          ],
                        ),
        
                        const SizedBox(
                          height: 30,
                        ),
                        Text("Username",
                          style: GoogleFonts.inter(
                            fontWeight:FontWeight.normal,
                            fontSize:17,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
        
                        const SizedBox(
                          height: 4,
                        ),
                        TextFormField(
                          controller: _usernameController,
                          decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(color: Colors.black,width: 3)
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                color: Colors.black,                 
                              ),
                            ),
                            hintText: "Enter username",
                            hintStyle: GoogleFonts.inter(
                              fontWeight:FontWeight.normal,
                              fontSize:15,
                              color:const Color.fromRGBO(0,0,0,0.7),
                            ),
                          ),
                          validator: (value) {
                            if(value!.trim().isEmpty){
                              return "Please enter username";
                            }else if(value.length < 6){
                              return "Username must be 6 character";
                            }else{
                              return null;
                            }
                          },
                        ),

                        // password TextFormField
                        const SizedBox(
                          height: 15,
                        ),
                        Text("Password",
                          style: GoogleFonts.inter(
                            fontWeight:FontWeight.normal,
                            fontSize:17,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
        
                        const SizedBox(
                          height: 4,
                        ),
                        TextFormField(
                          controller: _passwordController,
                          obscureText: flag,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: (){
                                setState(() {
                                  flag = !flag;
                                });
                              }, 
                              icon: Icon(
                                flag? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(color: Colors.black,width: 3)
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                color: Colors.black,                 
                              ),
                            ),
                            hintText: "Enter password",
                            hintStyle: GoogleFonts.inter(
                              fontWeight:FontWeight.normal,
                              fontSize:15,
                              color:const Color.fromRGBO(0,0,0,0.7),
                            ),
                          ),
                          validator: (value) {
                            if(value!.trim().isEmpty){
                              return "Please enter password";
                            }else if(value.length < 7){
                              return "Username must be 8 character";
                            }else if(!value.contains(RegExp(r'[A-Z]'))){
                              return "Password must be contains one upper case letter";
                            }else if(!value.contains(RegExp(r'[a-z]'))){
                              return "Password must be contains one lower case letter";
                            }else if(!value.contains(r'[!@#$%^&*(),.?":{}|<>]')){
                              return "Password must be contains one special character";
                            }else{
                              return null;
                            }
                          },
                        ),

                        const SizedBox(
                          height: 40,
                        ),  
                        // Elevated Button

                        GestureDetector(
                          onTap: () {
                            if(_usernameController.text == registerModelClass.username
                              && _passwordController.text == registerModelClass.password
                            ){

                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Login Successful",
                                    style: GoogleFonts.inter(
                                      fontSize:20,
                                      color:Colors.black,
                                      fontWeight:FontWeight.bold,
                                    ),
                                  ),
                                  backgroundColor: Colors.green,  
                                ),
                              );

                              Navigator.push(
                                context,MaterialPageRoute(
                                  builder: (context)=> const ToDoApp(),
                                ),
                              );
                            }else{
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Login Failed",
                                    style: GoogleFonts.inter(
                                      fontSize:20,
                                      color:Colors.black,
                                      fontWeight:FontWeight.bold,
                                    ),
                                  ),
                                  backgroundColor: Colors.red,  
                                ),
                              );
                            }
                            
                          },
                          child: Container(
                            height: 50,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.purpleAccent,
                            ),
                            child: Center(
                              child: Text("Login",                        
                              textAlign: TextAlign.center,
                                style: GoogleFonts.inter(                                 
                                  fontWeight:FontWeight.bold,
                                  fontSize:25,
                                  color:Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),

                        const SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create an account? ",
                              style: GoogleFonts.inter(
                                fontWeight:FontWeight.normal,
                                fontSize:15,
                                color:Colors.black,
                              ),
                            ),
                            TextButton(
                              onPressed: (){
                                Navigator.push(
                                  context,MaterialPageRoute(
                                    builder: (context)=> const Register(),
                                  ),
                                );
                              }, 
                              child: Text("Sign Up ",
                                style: GoogleFonts.inter(
                                  fontWeight:FontWeight.normal,
                                  fontSize:15,
                                  color:Colors.red,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}