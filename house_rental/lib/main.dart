import 'package:flutter/material.dart';
import 'package:house_rental/controller/near_location_controller.dart';
import 'package:house_rental/controller/screen1_controller.dart';
import 'package:provider/provider.dart';

import 'home_screen.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => HomeInfoController(),
        ),
        ChangeNotifierProvider(
          create: (context) => NearLocationController(),
        ),
      ],
      child: const MainApp(),
    ),
  );
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomeScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
